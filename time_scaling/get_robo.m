function [ xt, yt,xt_dot,yt_dot,kt,kt_dot,omega,velocity]=get_robo(x_weights,tan_weights,yo,to,t,tf)

 [ B0,B1,B2,B3,B4,B5] = get_bersntein_coeff(to,t,tf);
 [Bodot B1dot B2dot B3dot B4dot B5dot Boddot B1ddot B2ddot  B3ddot B4ddot B5ddot]=get_bernstein_differentials(to,t,tf);

     [coefot, coef1t, coef2t, coef3t, coef4t,coef5t ]=get_coeff(x_weights(1),x_weights(2),x_weights(3),x_weights(4),x_weights(5),x_weights(6),to,t,tf);  %These are the coeffecients of the �co-ordinate
 xt = B0*x_weights(1)+B1*x_weights(2)+B2*x_weights(3)+B3*x_weights(4)+B4*x_weights(5)+B5*x_weights(6); %% x co-ordinate
 xt_dot=Bodot*x_weights(1)+B1dot*x_weights(2)+B2dot*x_weights(3)+B3dot*x_weights(4)+B4dot*x_weights(5)+B5dot*x_weights(6); %% differential of x co-ordinate
 kt=B0*tan_weights(1)+B1*tan_weights(2)+B2*tan_weights(3)+B3*tan_weights(4)+B4*tan_weights(5)+B5*tan_weights(6);%%bernstein for tan(teta)
 kt_dot=Bodot*tan_weights(1)+B1dot*tan_weights(2)+B2dot*tan_weights(3)+B3dot*tan_weights(4)+B4dot*tan_weights(5)+B5dot*tan_weights(6); %% differntial of bernstein for tan(teta)
 yt = yo+tan_weights(1)*coefot+tan_weights(2)*coef1t+tan_weights(3)*coef2t+tan_weights(4)*coef3t+tan_weights(5)*coef4t+tan_weights(6)*coef5t; %% y co-ordinate , obtained by integrating xt_dot*kt as ydot=xdot*k is the non holonomic robot equation, the integration was done in mathematica
 yt_dot=xt_dot*kt; %% velocity in the direction of 'y'
 omega = kt_dot./(1+kt.^2); %% this is the angular velocity command to be given to the robot
 velocity=sqrt(xt_dot^2+yt_dot^2); %% this is the linear velocity command to be given to the robot
 
end