clc;
clear all;
%% declare robo parameters
xor=2;yor=2;xfr=12;yfr=12;xdotor=0.1;xddotor=0;xdotfr=0;kor=0;kdotor=0;kddotor=0;kfr=0; kdotfr=0;
thetar(1)=atan(kor);
to=0; tf=30; 
%% initialize for approximating with circles:
r_up=0.3;r_low=0.3;
%% middle circle
xo_cr(1)=xor;yo_cr(1)=yor;
%% upper circle
xo_ur(1)=xo_cr(1)+r_up*cos(thetar(1));
yo_ur(1)=yo_cr(1)+r_up*sin(thetar(1));
%% lower circle
xo_lr(1)=xo_cr(1)-r_low*cos(thetar(1));
yo_lr(1)=yo_cr(1)-r_low*sin(thetar(1));

%% declare obstacle parameters:
 xob=12;yob=2;xfb=2;yfb=14;xdotob=0;xddotob=0;xdotfb=0;kob=0;kdotob=0;kddotob=0;kfb=0; kdotfb=0;
%% get the weights for the bernstein traject for robo:
[x_weights_robo,tan_weights_robo]=get_weights(xor,yor,xfr,yfr,xdotor,xddotor,xdotfr,kor,kdotor,kddotor,kfr,kdotfr,to,tf);
%% get the weights for the bernstein traject for obstacle:
[x_weights_obstacle,tan_weights_obstacle]=get_weights(xob,yob,xfb,yfb,xdotob,xddotob,xdotfb,kob,kdotob,kddotob,kfb,kdotfb,to,tf);


 i = 1;R=1;
 for(t=to:0.1:tf)
     %% get velocity profile for robo
  [ xt_robo(i,:), yt_robo(i,:),xt_robodot(i,:) ,yt_robodot(i,:),kt_robo(i,:),kt_robodot(i,:),omega_robo(i,:),velocity_robo(i,:)]=get_robo(x_weights_robo,tan_weights_robo,yor,to,t,tf);

   i = i+1;
 end
 %% simulate with physics model:
 i=2;delt=0.1;
 xo_crdot=xt_robodot;yo_crdot=yt_robodot; omega_cr = kt_robodot./(1+kt_robo.^2); velocity_cr=sqrt(xo_crdot.^2+yo_crdot.^2);
 sc_centre=1;critical_distance_counter=1;
for(t=to:delt:tf)
  %% get robo circles
  [xo_cr(i,:),yo_cr(i,:),xo_ur(i,:),yo_ur(i,:),xo_lr(i,:),yo_lr(i,:),thetar(i,:),dist_upper_middle(i,:),dist_middle_lower(i,:)]=get_circles(xo_cr(i-1,:),yo_cr(i-1,:),xo_crdot(i-1),yo_crdot(i-1),xo_ur(i-1,:),yo_ur(i-1,:),xo_lr(i-1,:),yo_lr(i-1,:),thetar(i-1),omega_cr(i-1),r_up,r_low,delt,sc_centre);
   %% get velocity obstacle propogation
  [ xt_obst(i-1,:), yt_obst(i-1,:),xt_obstdot(i-1,:) ,yt_obstdot(i-1,:),kt_obst(i-1,:),kt_obstdot(i-1,:),omega_obst(i-1,:),velocity_obst(i-1,:)]=get_robo(x_weights_obstacle,tan_weights_obstacle,yob,to,t,tf);
%% distance between robo and obstacle:
distance(i,:)=sqrt((xo_cr(i,:)-xt_obst(i-1,:))^2+(yo_cr(i,:)-yt_obst(i-1,:))^2);
if(distance(i,:)<2.2*R)
    i_safe(critical_distance_counter)=i;
    i_safe_display= i_safe(critical_distance_counter);
    sc_centre=0.8;
    critical_distance_counter=critical_distance_counter+1;
  end
  hold off;
 robot_plot_with_sensor(xo_cr(i,:),yo_cr(i,:),0.5,0);
 hold on;
 robot_plot_with_sensor(xo_ur(i,:), yo_ur(i,:),0.5,0);
 hold on;
 robot_plot_with_sensor(xo_lr(i,:), yo_lr(i,:),0.5,0);
 hold on;
  robot_plot_with_sensor(xt_obst(i-1,:), yt_obst(i-1,:),0.5,1);
 axis([0 13 0 13]);
 hold on;
 plot(xo_cr(1:i,:),yo_cr(1:i,:),'m');
hold on;
plot(xt_obst(1:i-1,:), yt_obst(1:i-1,:),'b');
%pause;  
 frame(i-1) = getframe();
 i=i+1;
end

%% get the data:
i_safe(1)
robo_safe=[xo_cr(i_safe(1),:),yo_cr(i_safe(1),:),thetar(i_safe(1),:),xo_crdot(i_safe(1),:),yo_crdot(i_safe(1),:),omega_cr(i_safe(1),:)]
obst_safe=[xt_obst(i_safe(1)-1,:),yt_obst(i_safe(1)-1,:),xt_obstdot(i_safe(1)-1,:),yt_obstdot(i_safe(1)-1,:)]
movie2avi(frame,'avoiding','compression', 'None');
hold on;
plot(xo_cr,yo_cr,'m');
hold on;
plot( xt_robo, yt_robo);
%}


