(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29964,        713]
NotebookOptionsPosition[     29001,        679]
NotebookOutlinePosition[     29348,        694]
CellTagsIndexPosition[     29305,        691]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.706064646035533*^9, 3.7060646741901274`*^9}}],

Cell[BoxData[{
 RowBox[{"<<", "ToMATLAB.m"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Directory", "[", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", " ", "=", " ", 
   RowBox[{"OpenWrite", "[", "\"\<Tom.m\>\"", "]"}]}], 
  ";"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.706064691090483*^9, 3.706064704621482*^9}, {
  3.706064777716031*^9, 3.706064846915281*^9}, {3.706064989467309*^9, 
  3.70606500132969*^9}, {3.7060653352306805`*^9, 3.7060653375091887`*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"collcone", " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{"x0", " ", "-", " ", "x1"}], ")"}], "^", "2"}], " ", "+", 
         " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"y0", " ", "-", " ", "y1"}], ")"}], "^", "2"}], " ", "-", 
         " ", 
         RowBox[{"R", "^", "2"}]}], ")"}], "*", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"s0", "*", "x0dot"}], " ", "-", " ", "x1dot"}], ")"}], 
          "^", "2"}], " ", "+", " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"s0", "*", "y0dot"}], " ", "-", " ", "y1dot"}], ")"}], 
          "^", "2"}]}], ")"}]}], " ", "-", " ", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"s0", "*", "x0dot"}], " ", "-", " ", "x1dot"}], ")"}], 
          "*", 
          RowBox[{"(", 
           RowBox[{"x0", " ", "-", " ", "x1"}], ")"}]}], " ", "+", " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"s0", "*", "y0dot"}], " ", "-", " ", "y1dot"}], ")"}], 
          "*", 
          RowBox[{"(", 
           RowBox[{"y0", " ", "-", " ", "y1"}], ")"}]}]}], ")"}], "^", 
       "2"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collcone", ",", "f", ",", "\"\<collcone\>\""}], "]"}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"xoup", " ", "=", " ", 
     RowBox[{"xocr", " ", "+", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Cos", "[", "heading", "]"}]}]}]}], ";"}], "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"xoup", ",", "f", ",", "\"\<xoup\>\""}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"youp", " ", "=", " ", 
     RowBox[{"yocr", " ", "+", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Sin", "[", "heading", "]"}]}]}]}], ";"}], "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"youp", ",", "f", ",", "\"\<youp\>\""}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"xourdot", " ", "=", " ", 
     RowBox[{"xocrdot", " ", "-", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Sin", "[", "heading", "]"}], "*", "thetadot"}]}]}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"xourdot", ",", "f", ",", "\"\<xourdot\>\""}], "]"}], ";"}], " ",
    "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"yourdot", " ", "=", " ", 
     RowBox[{"yocrdot", " ", "+", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Cos", "[", "heading", "]"}], "*", "thetadot"}]}]}], ";"}], 
   " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"yourdot", ",", "f", ",", "\"\<yourdot\>\""}], "]"}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"collconeup", " ", "=", " ", 
     RowBox[{"collcone", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x0", " ", "->", " ", "xoup"}], ",", " ", 
        RowBox[{"y0", " ", "->", " ", "youp"}], ",", " ", 
        RowBox[{"x0dot", " ", "->", " ", "xourdot"}], ",", " ", 
        RowBox[{"y0dot", " ", "->", " ", "yourdot"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collconeup", ",", "f", ",", "\"\<collconeup\>\""}], "]"}], 
    ";"}], "\n", 
   RowBox[{
    RowBox[{"collconemiddle", " ", "=", " ", 
     RowBox[{"collcone", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x0", " ", "->", " ", "xocr"}], ",", " ", 
        RowBox[{"y0", " ", "->", " ", "yocr"}], ",", " ", 
        RowBox[{"x0dot", " ", "->", " ", "xocrdot"}], ",", " ", 
        RowBox[{"y0dot", " ", "->", " ", "yocrdot"}]}], "}"}]}]}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collconemiddle", ",", "f", ",", "\"\<collconemiddle\>\""}], 
     "]"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"xolr", " ", "=", " ", 
     RowBox[{"xocr", " ", "-", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Cos", "[", "heading", "]"}]}]}]}], ";"}], "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"xolr", ",", "f", ",", "\"\<xolr\>\""}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"yolr", " ", "=", " ", 
     RowBox[{"yocr", " ", "-", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Sin", "[", "heading", "]"}]}]}]}], ";"}], "\n", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"yolr", ",", "f", ",", "\"\<yolr\>\""}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"xolrdot", " ", "=", " ", 
     RowBox[{"xocrdot", " ", "+", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Sin", "[", "heading", "]"}], "*", "thetadot"}]}]}], ";"}], 
   " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"xolrdot", ",", "f", ",", "\"\<xolrdot\>\""}], "]"}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"yolrdot", " ", "=", " ", 
     RowBox[{"yocrdot", " ", "-", " ", 
      RowBox[{"rdelta", "*", 
       RowBox[{"Cos", "[", "heading", "]"}], "*", "thetadot"}]}]}], ";"}], 
   " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"yolrdot", ",", "f", ",", "\"\<yolrdot\>\""}], "]"}], ";"}], 
   "\n", 
   RowBox[{
    RowBox[{"collconelow", " ", "=", " ", 
     RowBox[{"collcone", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x0", " ", "->", " ", "xolr"}], ",", " ", 
        RowBox[{"y0", " ", "->", " ", "yolr"}], ",", " ", 
        RowBox[{"x0dot", " ", "->", " ", "xolrdot"}], ",", " ", 
        RowBox[{"y0dot", " ", "->", " ", "yolrdot"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collconelow", ",", "f", ",", "\"\<collconelow\>\""}], "]"}], 
    ";"}], "\n", 
   RowBox[{
    RowBox[{"collconeupeval", " ", "=", " ", 
     RowBox[{"collconeup", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
        RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
        RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
        RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
        RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
        RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
        RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
        RowBox[{"x1", " ", "->", " ", "7.4680"}], ",", " ", 
        RowBox[{"y1", " ", "->", " ", "7.2642"}], ",", " ", 
        RowBox[{"x1dot", " ", "->", " ", 
         RowBox[{"-", "0.6223"}]}], ",", " ", 
        RowBox[{"y1dot", " ", "->", " ", "0.9758"}], ",", " ", 
        RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collconeupeval", ",", "f", ",", "\"\<collconeupeval\>\""}], 
     "]"}], ";"}], "\n", 
   RowBox[{
    RowBox[{"collconemiddleeval", " ", "=", " ", 
     RowBox[{"collconemiddle", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
        RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
        RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
        RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
        RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
        RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
        RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
        RowBox[{"x1", " ", "->", " ", "7.4680"}], ",", " ", 
        RowBox[{"y1", " ", "->", " ", "7.2642"}], ",", " ", 
        RowBox[{"x1dot", " ", "->", " ", 
         RowBox[{"-", "0.6223"}]}], ",", " ", 
        RowBox[{"y1dot", " ", "->", " ", "0.9758"}], ",", " ", 
        RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{
     "collconemiddleeval", ",", "f", ",", "\"\<collconemiddleeval\>\""}], 
     "]"}], ";"}], "\n", 
   RowBox[{
    RowBox[{"collconeloweval", " ", "=", " ", 
     RowBox[{"collconelow", " ", "//.", " ", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
        RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
        RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
        RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
        RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
        RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
        RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
        RowBox[{"x1", " ", "->", " ", "7.4680"}], ",", " ", 
        RowBox[{"y1", " ", "->", " ", "7.2642"}], ",", " ", 
        RowBox[{"x1dot", " ", "->", " ", 
         RowBox[{"-", "0.6223"}]}], ",", " ", 
        RowBox[{"y1dot", " ", "->", " ", "0.9758"}], ",", " ", 
        RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"WriteMatlab", "[", 
     RowBox[{"collconeloweval", ",", "f", ",", "\"\<collconeloweval\>\""}], 
     "]"}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.6753020447719*^9, 3.675302056931427*^9}, 
   3.6753208120080047`*^9, {3.684683643896315*^9, 3.684683737116143*^9}, 
   3.6861647994061594`*^9, 3.7047824345011406`*^9, {3.704783071107767*^9, 
   3.70478307782967*^9}, {3.7047839203883133`*^9, 3.7047839997641373`*^9}, {
   3.7059132791199017`*^9, 3.70591338870717*^9}, {3.705913449154628*^9, 
   3.7059134813744707`*^9}, {3.7059139902085743`*^9, 3.705914109168378*^9}, {
   3.7059142126232953`*^9, 3.7059142287952204`*^9}, 3.705914288654644*^9, {
   3.7059143410916433`*^9, 3.7059143558884897`*^9}, {3.705916956838256*^9, 
   3.7059170047459955`*^9}, 3.705920663376257*^9, {3.705945116705908*^9, 
   3.705945190244114*^9}, 3.7060645946166077`*^9, {3.706065020043741*^9, 
   3.706065122547433*^9}, {3.7060654277912865`*^9, 3.7060656621173396`*^9}, {
   3.706065706446313*^9, 3.706065907192382*^9}, {3.706065955258437*^9, 
   3.7060659955221653`*^9}, {3.7060660339613304`*^9, 
   3.7060660403386135`*^9}, {3.7060662575541916`*^9, 3.7060665466641245`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeupeval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Reduce", "[", 
  RowBox[{
   RowBox[{"collconeupeval", ">", "0"}], ",", "s0"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconemiddleeval", ">", "0"}], ",", "s0"}], "]"}], ",", 
    "f"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeloweval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.70591698000158*^9, 3.7059170064720945`*^9}, {
   3.706066614230836*^9, 3.7060666377430587`*^9}, {3.7060667269462585`*^9, 
   3.7060667720398107`*^9}, {3.70606680422983*^9, 3.7060668074271917`*^9}, {
   3.7060675789886875`*^9, 3.7060676120129347`*^9}, 3.706067737100643*^9, {
   3.706071890837273*^9, 3.706071891290393*^9}}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,969,131,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644875891576`*^9, 3.7060647255981765`*^9, 3.7060648021819773`*^9, 
   3.706064852402917*^9, 3.7060651331244392`*^9, 3.7060651715973873`*^9, {
   3.706065239696667*^9, 3.7060652612005463`*^9}, 3.70606534728767*^9, 
   3.706065676205345*^9, 3.70606573684927*^9, 3.7060659124904966`*^9, 
   3.7060660449162645`*^9, 3.706066555700079*^9, {3.706066644078323*^9, 
   3.706066651869464*^9}, 3.7060668139606805`*^9, {3.706067567563535*^9, 
   3.70606760210962*^9}, 3.706068048726118*^9, 3.706068281494667*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,970,132,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644875891576`*^9, 3.7060647255981765`*^9, 3.7060648021819773`*^9, 
   3.706064852402917*^9, 3.7060651331244392`*^9, 3.7060651715973873`*^9, {
   3.706065239696667*^9, 3.7060652612005463`*^9}, 3.70606534728767*^9, 
   3.706065676205345*^9, 3.70606573684927*^9, 3.7060659124904966`*^9, 
   3.7060660449162645`*^9, 3.706066555700079*^9, {3.706066644078323*^9, 
   3.706066651869464*^9}, 3.7060668139606805`*^9, {3.706067567563535*^9, 
   3.70606760210962*^9}, 3.706068048726118*^9, 3.7060682815102935`*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,971,133,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644875891576`*^9, 3.7060647255981765`*^9, 3.7060648021819773`*^9, 
   3.706064852402917*^9, 3.7060651331244392`*^9, 3.7060651715973873`*^9, {
   3.706065239696667*^9, 3.7060652612005463`*^9}, 3.70606534728767*^9, 
   3.706065676205345*^9, 3.70606573684927*^9, 3.7060659124904966`*^9, 
   3.7060660449162645`*^9, 3.706066555700079*^9, {3.706066644078323*^9, 
   3.706066651869464*^9}, 3.7060668139606805`*^9, {3.706067567563535*^9, 
   3.70606760210962*^9}, 3.706068048726118*^9, 3.7060682815415473`*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{"collconeupeval", " ", "=", " ", 
   RowBox[{"collconeup", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.3054"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.5176"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6273"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9296"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{"collconeupeval", ",", "f", ",", "\"\<collconeupeval\>\""}], "]"}],
   ";"}], "\n", 
 RowBox[{
  RowBox[{"collconemiddleeval", " ", "=", " ", 
   RowBox[{"collconemiddle", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.3054"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.5176"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6273"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9296"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
   "collconemiddleeval", ",", "f", ",", "\"\<collconemiddleeval\>\""}], "]"}],
   ";"}], "\n", 
 RowBox[{
  RowBox[{"collconeloweval", " ", "=", " ", 
   RowBox[{"collconelow", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.3054"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.5176"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6273"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9296"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{"collconeloweval", ",", "f", ",", "\"\<collconeloweval\>\""}], 
   "]"}], ";"}]}], "Input",
 CellChangeTimes->{
  3.7059398946022205`*^9, {3.705944984116324*^9, 3.705945052041209*^9}, {
   3.705945204666939*^9, 3.7059452088761797`*^9}, 3.706067560362217*^9, {
   3.7060676473268156`*^9, 3.706067670428788*^9}, {3.706067768009676*^9, 
   3.7060678850180717`*^9}, {3.706067938322075*^9, 3.7060680260483093`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeupeval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconemiddleeval", ">", "0"}], ",", "s0"}], "]"}], ",", 
    "f"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeloweval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}]}], "Input",
 CellChangeTimes->{
  3.7059401311117477`*^9, {3.706068042282868*^9, 3.7060680441035647`*^9}}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,978,134,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644876672883`*^9, 3.7060647257172556`*^9, 3.7060648022980537`*^9, 
   3.706064852515992*^9, 3.706065133240517*^9, 3.706065171714466*^9, {
   3.706065239837761*^9, 3.706065261319626*^9}, 3.7060653474117517`*^9, 
   3.706065676314722*^9, 3.7060657369586487`*^9, 3.706065912599877*^9, 
   3.7060660450412717`*^9, 3.7060665558094587`*^9, {3.706066644182438*^9, 
   3.706066651994467*^9}, 3.706066814070059*^9, {3.706067567652094*^9, 
   3.7060676022212076`*^9}, 3.706068048852701*^9, 3.706068281572797*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,979,135,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644876672883`*^9, 3.7060647257172556`*^9, 3.7060648022980537`*^9, 
   3.706064852515992*^9, 3.706065133240517*^9, 3.706065171714466*^9, {
   3.706065239837761*^9, 3.706065261319626*^9}, 3.7060653474117517`*^9, 
   3.706065676314722*^9, 3.7060657369586487`*^9, 3.706065912599877*^9, 
   3.7060660450412717`*^9, 3.7060665558094587`*^9, {3.706066644182438*^9, 
   3.706066651994467*^9}, 3.706066814070059*^9, {3.706067567652094*^9, 
   3.7060676022212076`*^9}, 3.706068048852701*^9, 3.706068281604048*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,980,136,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644876672883`*^9, 3.7060647257172556`*^9, 3.7060648022980537`*^9, 
   3.706064852515992*^9, 3.706065133240517*^9, 3.706065171714466*^9, {
   3.706065239837761*^9, 3.706065261319626*^9}, 3.7060653474117517`*^9, 
   3.706065676314722*^9, 3.7060657369586487`*^9, 3.706065912599877*^9, 
   3.7060660450412717`*^9, 3.7060665558094587`*^9, {3.706066644182438*^9, 
   3.706066651994467*^9}, 3.706066814070059*^9, {3.706067567652094*^9, 
   3.7060676022212076`*^9}, 3.706068048852701*^9, 3.706068281635301*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"collconeupeval", " ", "=", " ", 
   RowBox[{"collconeup", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.6306"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.0107"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6173"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9790"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{"collconeupeval", ",", "f", ",", "\"\<collconeupeval\>\""}], "]"}],
   ";"}], "\n", 
 RowBox[{
  RowBox[{"collconemiddleeval", " ", "=", " ", 
   RowBox[{"collconemiddle", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.6306"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.0107"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6173"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9790"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
   "collconemiddleeval", ",", "f", ",", "\"\<collconemiddleeval\>\""}], "]"}],
   ";"}], "\n", 
 RowBox[{
  RowBox[{"collconeloweval", " ", "=", " ", 
   RowBox[{"collconelow", " ", "//.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xocr", " ", "->", " ", "6.3767"}], ",", " ", 
      RowBox[{"yocr", " ", "->", " ", "6.1141"}], ",", " ", 
      RowBox[{"xocrdot", " ", "->", " ", "0.5417"}], ",", " ", 
      RowBox[{"yocrdot", " ", "->", " ", "0.7603"}], ",", " ", 
      RowBox[{"rdelta", " ", "->", " ", "0.3"}], ",", " ", "\n", "    ", 
      RowBox[{"heading", " ", "->", " ", "0.9514"}], ",", " ", 
      RowBox[{"thetadot", " ", "->", " ", "0.0072"}], ",", " ", 
      RowBox[{"x1", " ", "->", " ", "7.6306"}], ",", " ", 
      RowBox[{"y1", " ", "->", " ", "7.0107"}], ",", " ", 
      RowBox[{"x1dot", " ", "->", " ", 
       RowBox[{"-", "0.6173"}]}], ",", " ", 
      RowBox[{"y1dot", " ", "->", " ", "0.9790"}], ",", " ", 
      RowBox[{"R", " ", "->", " ", "1"}]}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{"collconeloweval", ",", "f", ",", "\"\<collconeloweval\>\""}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeupeval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconemiddleeval", ">", "0"}], ",", "s0"}], "]"}], ",", 
    "f"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteMatlab", "[", 
   RowBox[{
    RowBox[{"Reduce", "[", 
     RowBox[{
      RowBox[{"collconeloweval", ">", "0"}], ",", "s0"}], "]"}], ",", "f"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Close", "[", "f", "]"}], ";"}]}], "Input",
 CellChangeTimes->{
  3.7059407695802665`*^9, {3.7059452495945086`*^9, 3.705945293426016*^9}, {
   3.706065149128046*^9, 3.706065166961319*^9}, {3.70606810544816*^9, 
   3.706068108276393*^9}, {3.706068141982074*^9, 3.706068277043106*^9}}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,987,137,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644877610397`*^9, 3.706064725826827*^9, 3.706064802407626*^9, 
   3.706064852623068*^9, 3.7060651333475904`*^9, 3.7060651718295436`*^9, {
   3.70606523995684*^9, 3.706065261424694*^9}, 3.706065347526828*^9, 
   3.706065676424103*^9, 3.7060657370680285`*^9, 3.706065912709257*^9, 
   3.706066045150652*^9, 3.7060665559188385`*^9, {3.7060666442918177`*^9, 
   3.7060666521038465`*^9}, 3.706066814198013*^9, {3.706067567784181*^9, 
   3.7060676023432884`*^9}, 3.7060680489772835`*^9, 3.70606828166655*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,988,138,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644877610397`*^9, 3.706064725826827*^9, 3.706064802407626*^9, 
   3.706064852623068*^9, 3.7060651333475904`*^9, 3.7060651718295436`*^9, {
   3.70606523995684*^9, 3.706065261424694*^9}, 3.706065347526828*^9, 
   3.706065676424103*^9, 3.7060657370680285`*^9, 3.706065912709257*^9, 
   3.706066045150652*^9, 3.7060665559188385`*^9, {3.7060666442918177`*^9, 
   3.7060666521038465`*^9}, 3.706066814198013*^9, {3.706067567784181*^9, 
   3.7060676023432884`*^9}, 3.7060680489772835`*^9, 3.7060682816978035`*^9}],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,989,139,33317544416742108879,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7060644877610397`*^9, 3.706064725826827*^9, 3.706064802407626*^9, 
   3.706064852623068*^9, 3.7060651333475904`*^9, 3.7060651718295436`*^9, {
   3.70606523995684*^9, 3.706065261424694*^9}, 3.706065347526828*^9, 
   3.706065676424103*^9, 3.7060657370680285`*^9, 3.706065912709257*^9, 
   3.706066045150652*^9, 3.7060665559188385`*^9, {3.7060666442918177`*^9, 
   3.7060666521038465`*^9}, 3.706066814198013*^9, {3.706067567784181*^9, 
   3.7060676023432884`*^9}, 3.7060680489772835`*^9, 3.7060682817134304`*^9}]
}, Open  ]]
},
WindowSize->{1350, 686},
WindowMargins->{{-8, Automatic}, {Automatic, 0}},
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 191, 4, 30, "Input"],
Cell[752, 26, 498, 10, 88, "Input"],
Cell[1253, 38, 10446, 253, 658, "Input"],
Cell[CellGroupData[{
Cell[11724, 295, 1116, 30, 88, "Input"],
Cell[12843, 327, 831, 14, 23, "Message"],
Cell[13677, 343, 833, 14, 23, "Message"],
Cell[14513, 359, 833, 14, 23, "Message"]
}, Open  ]],
Cell[15361, 376, 3581, 75, 183, "Input"],
Cell[CellGroupData[{
Cell[18967, 455, 741, 23, 69, "Input"],
Cell[19711, 480, 832, 14, 23, "Message"],
Cell[20546, 496, 832, 14, 23, "Message"],
Cell[21381, 512, 832, 14, 23, "Message"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22250, 531, 4224, 97, 259, "Input"],
Cell[26477, 630, 832, 14, 23, "Message"],
Cell[27312, 646, 835, 14, 23, "Message"],
Cell[28150, 662, 835, 14, 23, "Message"]
}, Open  ]]
}
]
*)

