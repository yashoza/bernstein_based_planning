%% Symbols and intersections
syms x0 x1 y0 y1 R s0 x0dot y0dot x1dot y1dot xocr yocr rdelta heading xocrdot yocrdot thetadot;
syms xolr yolr 
intersection = [-inf,inf];
%% Collision cone equations;
collcone=(-1).*((x0+(-1).*x1).*(s0.*x0dot+(-1).*x1dot)+(y0+(-1).*y1).*(s0.*y0dot+ ...
  (-1).*y1dot)).^2+((-1).*R.^2+(x0+(-1).*x1).^2+(y0+(-1).*y1).^2).*((s0.* ...
  x0dot+(-1).*x1dot).^2+(s0.*y0dot+(-1).*y1dot).^2);
xoup=xocr+rdelta.*cos(heading);
youp=yocr+rdelta.*sin(heading);
xourdot=xocrdot+(-1).*rdelta.*thetadot.*sin(heading);
yourdot=yocrdot+rdelta.*thetadot.*cos(heading);
collconeup=(-1).*(((-1).*y1dot+s0.*(yocrdot+rdelta.*thetadot.*cos(heading))).*((-1) ...
  .*y1+yocr+rdelta.*sin(heading))+((-1).*x1+xocr+rdelta.*cos(heading)).*(( ...
  -1).*x1dot+s0.*(xocrdot+(-1).*rdelta.*thetadot.*sin(heading)))).^2+((-1) ...
  .*R.^2+((-1).*x1+xocr+rdelta.*cos(heading)).^2+((-1).*y1+yocr+rdelta.* ...
  sin(heading)).^2).*(((-1).*y1dot+s0.*(yocrdot+rdelta.*thetadot.*cos( ...
  heading))).^2+((-1).*x1dot+s0.*(xocrdot+(-1).*rdelta.*thetadot.*sin( ...
  heading))).^2);
collconemiddle=(-1).*(((-1).*x1+xocr).*((-1).*x1dot+s0.*xocrdot)+((-1).*y1+yocr).*((-1) ...
  .*y1dot+s0.*yocrdot)).^2+((-1).*R.^2+((-1).*x1+xocr).^2+((-1).*y1+yocr) ...
  .^2).*(((-1).*x1dot+s0.*xocrdot).^2+((-1).*y1dot+s0.*yocrdot).^2);
xolr=xocr+(-1).*rdelta.*cos(heading);
yolr=yocr+(-1).*rdelta.*sin(heading);
xolrdot=xocrdot+rdelta.*thetadot.*sin(heading);
yolrdot=yocrdot+(-1).*rdelta.*thetadot.*cos(heading);
collconelow=(-1).*(((-1).*y1dot+s0.*(yocrdot+(-1).*rdelta.*thetadot.*cos(heading))) ...
  .*((-1).*y1+yocr+(-1).*rdelta.*sin(heading))+((-1).*x1+xocr+(-1).* ...
  rdelta.*cos(heading)).*((-1).*x1dot+s0.*(xocrdot+rdelta.*thetadot.*sin( ...
  heading)))).^2+((-1).*R.^2+((-1).*x1+xocr+(-1).*rdelta.*cos(heading)) ...
  .^2+((-1).*y1+yocr+(-1).*rdelta.*sin(heading)).^2).*(((-1).*y1dot+s0.*( ...
  yocrdot+(-1).*rdelta.*thetadot.*cos(heading))).^2+((-1).*x1dot+s0.*( ...
  xocrdot+rdelta.*thetadot.*sin(heading))).^2);
%% Collision cone equations for 3 circles wrt to obstacle center circle;
collconeupeval=subs(collconeup,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.4680,7.2642,-0.6223,0.9758,1]);
collconemiddleeval=subs(collconemiddle,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.4680,7.2642,-0.6223,0.9758,1]);
collconeloweval=subs(collconelow,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.4680,7.2642,-0.6223,0.9758,1]);
msolve = double(solve(collconeupeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve = double(solve(collconemiddleeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve  = double(solve(collconeloweval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
%% Collision cone equations for 3 circles wrt to obstacle upper circle;
collconeupeval=subs(collconeup,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.3054,7.5176,-0.6273,0.9296,1]);
collconemiddleeval=subs(collconemiddle,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.3054,7.5176,-0.6273,0.9296,1]);
collconeloweval=subs(collconelow,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.3054,7.5176,-0.6273,0.9296,1]);
msolve = double(solve(collconeupeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve = double(solve(collconemiddleeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve = double(solve(collconeloweval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
%% Collision cone equations for 3 circles wrt to obstacle lower circle;
collconeupeval=subs(collconeup,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.6306,7.0107,-0.6173,0.9790,1]);
collconemiddleeval=subs(collconemiddle,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.6306,7.0107,-0.6173,0.9790,1]);
collconeloweval=subs(collconelow,[xocr,yocr,xocrdot,yocrdot,rdelta,heading,thetadot,x1,y1,x1dot,y1dot,R],[6.3767,6.1141,0.5417,0.7603,0.3,0.9514,0.0072,7.6306,7.0107,-0.6173,0.9790,1]);
msolve = double(solve(collconeupeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve = double(solve(collconemiddleeval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
msolve = double(solve(collconeloweval==0,s0));
if(subs(collconeupeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end
%% Print intersection
disp(intersection);
[M,I] = min((intersection -1).^2);
scale = intersection(I)