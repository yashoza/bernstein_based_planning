clc;
clear all;
%% declare robo parameters
xor=2;yor=2;xfr=12;yfr=12;xdotor=0.1;xddotor=0;xdotfr=0;kor=0;kdotor=0;kddotor=0;kfr=0; kdotfr=0;
thetar(1)=atan(kor);
to=0; tf=30; 
%% get the weights for the bernstein traject for robo:
[x_weights_robo,tan_weights_robo]=get_weights(xor,yor,xfr,yfr,xdotor,xddotor,xdotfr,kor,kdotor,kddotor,kfr,kdotfr,to,tf);

i = 1;R=1;
 for(t=to:0.1:tf)
     %% get velocity profile for robo
  [ xt_robo(i,:), yt_robo(i,:),xt_robodot(i,:) ,yt_robodot(i,:),kt_robo(i,:),kt_robodot(i,:),omega_robo(i,:),velocity_robo(i,:)]=get_robo(x_weights_robo,tan_weights_robo,yor,to,t,tf);

   i = i+1;
 end
 
 xo_robo=xo_prev+xo_crdot*delt;
 yo_cr=yo_cr_prev+yo_crdot*delt;