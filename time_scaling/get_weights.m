function [x_weights,tan_weights]=get_weights(xo,yo,xf,yf,xdoto,xddoto,xdotf,ko,kdoto,kddoto,kf,kdotf,to,tf)

xc1=((xf+xo)/2);yc1=((yf+yo)/2);tc=to+0.5*(tf-to); 
Ax = [xo xf xdoto xdotf xddoto xc1];
x_weights = xcomptraj([Ax],to,tc,tf);

%% get the coefficients to calculate y at t=tf;
t = tf;
[coefotf coef1tf coef2tf coef3tf coef4tf coef5tf ]=get_coeff(x_weights(1),x_weights(2),x_weights(3),x_weights(4),x_weights(5),x_weights(6),to,t,tf);
%% get the coefficients to calculate y at t=tc;
t = tc;
[coefotc1 coef1tc1 coef2tc1 coef3tc1 coef4tc1 coef5tc1 ]=get_coeff(x_weights(1),x_weights(2),x_weights(3),x_weights(4),x_weights(5),x_weights(6),to,t,tf);

%% --get differentials of bernstein coeffs at t=to:
t=to;
[Bodoto B1doto B2doto B3doto B4doto B5doto Boddoto B1ddoto B2ddoto  B3ddoto B4ddoto B5ddoto]=get_bernstein_differentials(to,t,tf);
%% --get differentials of bernstein coeffs at t=tf:
t=tf;
[Bodotf B1dotf B2dotf B3dotf B4dotf B5dotf Boddotf B1ddotf B2ddotf  B3ddotf B4ddotf B5ddotf]=get_bernstein_differentials(to,t,tf);


  %% the following lines of code implement the continuity,
  %% continuity are basically of the form AX=B, where A is A1 here and 'X' are the weights of the bernstein
  %% here 'X' is XX and the weights of the bernstein here are that of 'y' i.e: 
  %% y = yo+ko*coefot+k1*coef1t+k2*coef2t+k3*coef3t+k4*coef4t+kf*coef5t, we determine k1,k2,k3,k4 here 
  %% ko and kf are already initialized. 
  %% coefot,coef1t,coef2t,coef3t,coef4t,coef5t are basically functions of weights of the 'x' bernstein  and time
  
A1 = [B1doto B2doto B3doto B4doto;...  %% initial condition kdot
       B1dotf B2dotf B3dotf B4dotf;...   %% final condition kdot
       coef1tf coef2tf coef3tf coef4tf;...       %% final condition for 'y', i.e yf
      coef1tc1 coef2tc1 coef3tc1 coef4tc1]; %% condition for 'y' at t=tc. i.e yc
  
C1 = kdoto-Bodoto*ko-B5doto*kf; %% row1 of the 'B' array correspomding to the first condition of A1
C2 = kdotf-Bodotf*ko-B5dotf*kf; %% row2 of the 'B' array correspomding to the first condition of A2
C3 = yf-ko*coefotf-kf*coef5tf-yo; %% row3 of the 'B' array correspomding to the first condition of A3
C4 = yc1-ko*coefotc1-kf*coef5tc1-yo;%% row4 of the 'B' array correspomding to the first condition of A4

%kddoto = 0;
C5 =  kddoto-Boddoto*ko-B5ddoto*kf;
 C = [C1;C2;C3;C4];
%C = [C1;C3;C4];
%%XX = (A1)\C;
%% ko,k1,k2,k3,k4,kf, are bernstein coeffecients of tan(theta),
XX=pinv(A1)*C;
tan_weights=[ko;XX(1);XX(2);XX(3);XX(4);kf];
end
