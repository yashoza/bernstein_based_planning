function[xo_cr,yo_cr]=get_middle_circle(xo_cr_prev,yo_cr_prev,xo_crdot,yo_crdot,delt)

xo_cr=xo_cr_prev+xo_crdot*delt;
 yo_cr=yo_cr_prev+yo_crdot*delt;
 
end