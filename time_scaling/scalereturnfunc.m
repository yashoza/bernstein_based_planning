

function [scale] = scalereturnfunc(ax,ay,axdot,aydot,bx,by,bxdot,bydot,Radius)
%% Symbols and intersections
syms x1 y1 R s0 x1dot y1dot xocr yocr xocrdot yocrdot;
syms xolr yolr
intersection = [-inf,inf];
%% Finding the other terms
bxc = bx;
byc = by;
bxcdot = bxdot;
bycdot = bydot;
%% Collision cone equations
collconemiddle=(-1).*(((-1).*x1+xocr).*((-1).*x1dot+s0.*xocrdot)+((-1).*y1+yocr).*((-1) ...
    .*y1dot+s0.*yocrdot)).^2+((-1).*R.^2+((-1).*x1+xocr).^2+((-1).*y1+yocr) ...
    .^2).*(((-1).*x1dot+s0.*xocrdot).^2+((-1).*y1dot+s0.*yocrdot).^2);
%% Collision cone equations for 3 circles wrt to obstacle center circle;
collconemiddleeval=subs(collconemiddle,[xocr,yocr,xocrdot,yocrdot,x1,y1,x1dot,y1dot,R],[ax,ay,axdot,aydot,bxc,byc,bxcdot,bycdot,Radius]);
msolve = double(solve(collconemiddleeval==0,s0));
if(subs(collconemiddleeval,s0,(sum(msolve)/2)) > 0)
    intersection = range_intersection(intersection,[min(msolve),max(msolve)]);
else
    intersection = range_intersection(intersection,[-inf,min(msolve),max(msolve),inf]);
end

%% Print intersection
[~,I] = min((intersection -1).^2);
scale = intersection(I);
disp(intersection);
end

