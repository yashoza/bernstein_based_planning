%% Clear Workspace and Commands
clear;
clc;
%% Simulation parameters
to = 0; % Simulation start time
tf = 20; % Simulation End time
delt=0.1; % Time step of simulation
endt = tf; % Extended End time
t=0; % Initial time
%% Path of robot

[x_weights_robo,tan_weights_robo] = create_Agent_func(2,2,12,12,0,0,0,0,0,0,0,0,0,20); % weights of robot 

ai = 1; % Index of of Current Path and orientation memory matrix  
al = 1.5; % Length of robo
ab = 1; % Width of robo
radiusrobo = sqrt(power((al/2),2)+power((ab/2),2)); % Radius of single circle encompassing robot
ahorizon = tf; % Total end time of robots simulation (depands on scaling) 
aprevscale =1; % Previous scale
acurrentscale = 1; % Current scale

%% Robots Memory vectors

thetar(1)=atan(0); % Initial-Heading is stored in Theta memory matrix
xo_cr(1)=2; % Initial X-coordinate stored in X memory matrix                                   
yo_cr(1)=2; % Initial Y-Coordinate stored in Y memory matrix

%% Path of obstacle

[x_weights_obstacle,tan_weights_obstacle]=create_Agent_func(12,2,2,14,0,0,0,0,0,0,0,0,0,20); % weights of obstacle
bi = 1; % Index of of Current Path and orientation memory matrix  
bl = 1.5; % Length of obstacle
bb = 1; % Width of obstacle
radiusobst = sqrt(power((bl/2),2)+power((bb/2),2)); % Radius of single circle encompassing obstacle
bhorizon = tf; % Total end time of obstacles simulation (depands on scaling)
bprevscale = 1; % perivious scale
bcurrentscale = 1; % current scale

%% Obstacles Memory Vectors

thetab(1)=atan(0); % Initial-Heading is stored in Theta memory matrix
xo_cb(1)=12; % Initial X-coordinate stored in X memory matrix                                    
yo_cb(1)=2; % Initial Y-Coordinate stored in Y memory matrix

%% Rectangular Onject Transformations
syms xcentre ycentre director;

rectx = [(al/2),(al/2),-(al/2),-(al/2)];
recty = [-(ab/2),(ab/2),(ab/2),-(ab/2)];
plot_rectx = [xcentre+(rectx(1)*cos(director) - recty(1)*sin(director)),xcentre+(rectx(2)*cos(director) - recty(2)*sin(director)),xcentre+(rectx(3)*cos(director) - recty(3)*sin(director)),xcentre+(rectx(4)*cos(director) - recty(4)*sin(director))];
plot_recty = [ycentre+(rectx(1)*sin(director) + recty(1)*cos(director)),ycentre+(rectx(2)*sin(director) + recty(2)*cos(director)),ycentre+(rectx(3)*sin(director) + recty(3)*cos(director)),ycentre+(rectx(4)*sin(director) + recty(4)*cos(director))];


%%  Velocity Obstracle paramters

R = radiusrobo + radiusobst + 0.02; % minkowski radius

%% Plot of original path of Scaled robot
i=1;
for t=to:delt:tf
    [plot_xt_robo(i,:),plot_yt_robo(i,:),plot_velx(i,:),plot_vely(i,:),~,~,~,~]=get_robo(x_weights_robo,tan_weights_robo,yo_cr(1),to,t,tf);
    i=i+1;
end
t=0;
%% 

prevtempdist=0;
counter = 0;
atime = to;
btime = to;
plotter = figure(1);
axis([0 15 0 15]);
hold on;
myVideo = VideoWriter('one_circle.avi','Uncompressed AVI');
open(myVideo);
while(t<=endt)
    tempdist=sqrt((xo_cr(ai,:)-xo_cb(bi,:))^2+(yo_cr(ai,:)-yo_cb(bi,:))^2);
    if((tempdist<2.2*R))
        counter = counter +1;
        if(counter ==1)
            [~,~,velbx,velby,~,~,omegab,~]=get_robo(x_weights_obstacle,tan_weights_obstacle,yo_cb(1),to,btime,tf);
            [~,~,velax,velay,~,~,omegaa,~]=get_robo(x_weights_robo,tan_weights_robo,yo_cr(1),to,atime,tf);
            tempscale = scalereturnfunc(xo_cr(ai,:),yo_cr(ai,:),velax,velay,xo_cb(bi,:),yo_cb(bi,:),velbx,velby,R);
            inittime = t;
        end
        aprevscale = acurrentscale;
        acurrentscale = tempscale;
        bprevscale = bcurrentscale;
        bcurrentscale = 1;
        prevtempdist = tempdist;
        figure(1);
        cla;
        bhorizon = t + ((bhorizon-t)/(bcurrentscale/bprevscale));
        ahorizon = t + ((ahorizon-t)/(acurrentscale/aprevscale));
        if(t <= bhorizon)
            bdelt = delt*bcurrentscale;
            btime = btime + bdelt;
            [~,~,velbx,velby,~,~,omegab,~]=get_robo(x_weights_obstacle,tan_weights_obstacle,yo_cb(1),to,btime,tf);
            [xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)]=get_circles(xo_cb(bi,:),yo_cb(bi,:),velbx,velby,thetab(bi),omegab,delt,bcurrentscale);
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)])),'y');
            robot_plot_with_sensor(xo_cb(bi+1,:),yo_cb(bi+1,:),radiusobst,0);
            plot(xo_cb(1:bi+1,:), yo_cb(1:bi+1,:),'b');
            bi=bi+1;
        else
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cb(bi,:),yo_cb(bi,:),thetab(bi,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cb(bi,:),yo_cb(bi,:),thetab(bi,:)])),'y');
            robot_plot_with_sensor(xo_cb(bi,:),yo_cb(bi,:),radiusobst,0);
            plot(xo_cb(1:bi,:), yo_cb(1:bi,:),'b');
        end
        if(t<= ahorizon)
            finaltime = t;
            adelt = delt*acurrentscale;
            atime = atime + adelt;
            [~,~,velax,velay,~,~,omegaa,~]=get_robo(x_weights_robo,tan_weights_robo,yo_cr(1),to,atime,tf);
            plot_mod_velx(ai,:) = velax;
            [xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)]=get_circles(xo_cr(ai,:),yo_cr(ai,:),velax,velay,thetar(ai),omegaa,delt,acurrentscale);
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)])),'y');
            robot_plot_with_sensor(xo_cr(ai+1,:),yo_cr(ai+1,:),radiusrobo,0);
            plot(xo_cr(1:ai+1,:),yo_cr(1:ai+1,:),'r');
            ai=ai+1;
        else
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cr(ai,:),yo_cr(ai,:),thetar(ai,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cr(ai,:),yo_cr(ai,:),thetar(ai,:)])),'y');
            robot_plot_with_sensor(xo_cr(ai,:),yo_cr(ai,:),radiusrobo,0);
            plot(xo_cr(1:ai,:),yo_cr(1:ai,:),'r');
        end
    else
        aprevscale = acurrentscale;
        acurrentscale = 1;
        bprevscale = bcurrentscale;
        bcurrentscale = 1;
        prevtempdist = tempdist;
        figure(1);
        cla;
        bhorizon = t + ((bhorizon-t)/(bcurrentscale/bprevscale));
        ahorizon = t + ((ahorizon-t)/(acurrentscale/aprevscale));
        if(t <= bhorizon)
            bdelt = delt*bcurrentscale;
            btime = btime + bdelt;
            [~,~,velbx,velby,~,~,omegab,~]=get_robo(x_weights_obstacle,tan_weights_obstacle,yo_cb(1),to,btime,tf);
            [xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)]=get_circles(xo_cb(bi,:),yo_cb(bi,:),velbx,velby,thetab(bi),omegab,delt,bcurrentscale);
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cb(bi+1,:),yo_cb(bi+1,:),thetab(bi+1,:)])),'y');
            robot_plot_with_sensor(xo_cb(bi+1,:),yo_cb(bi+1,:),radiusobst,0);
            plot(xo_cb(1:bi+1,:), yo_cb(1:bi+1,:),'b');
            bi=bi+1;
        else
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cb(bi,:),yo_cb(bi,:),thetab(bi,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cb(bi,:),yo_cb(bi,:),thetab(bi,:)])),'y');
            robot_plot_with_sensor(xo_cb(bi,:),yo_cb(bi,:),radiusobst,0);
            plot(xo_cb(1:bi,:), yo_cb(1:bi,:),'b');
        end
        if(t<= ahorizon)
            adelt = delt*acurrentscale;
            atime = atime + adelt;
            [~,~,velax,velay,~,~,omegaa,~]=get_robo(x_weights_robo,tan_weights_robo,yo_cr(1),to,atime,tf);
            plot_mod_velx(ai,:) = velax;
            [xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)]=get_circles(xo_cr(ai,:),yo_cr(ai,:),velax,velay,thetar(ai),omegaa,delt,acurrentscale);
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cr(ai+1,:),yo_cr(ai+1,:),thetar(ai+1,:)])),'y');
            robot_plot_with_sensor(xo_cr(ai+1,:),yo_cr(ai+1,:),radiusrobo,0);
            plot(xo_cr(1:ai+1,:),yo_cr(1:ai+1,:),'r');
            ai=ai+1;
        else
            fill(double(subs(plot_rectx,[xcentre,ycentre,director],[xo_cr(ai,:),yo_cr(ai,:),thetar(ai,:)])),double(subs(plot_recty,[xcentre,ycentre,director],[xo_cr(ai,:),yo_cr(ai,:),thetar(ai,:)])),'y');
            robot_plot_with_sensor(xo_cr(ai,:),yo_cr(ai,:),radiusrobo,0);
            plot(xo_cr(1:ai,:),yo_cr(1:ai,:),'r');
        end
    end
    t = t + delt;
    pause(0.01);
    if(bhorizon<ahorizon)
        endt = ahorizon;
    else
        endt = bhorizon;
    end
    F = getframe;
    %writeVideo(myVideo,F);
end
plot_time = to:delt:tf;
plot_a_time = to:delt:endt;
hold on;
plot(xo_cr,yo_cr,'m');
hold on;
plot(plot_xt_robo,plot_yt_robo);
close(myVideo);
figure(3);
axis([0 25 0 2]);
hold on;
plot(plot_time,plot_velx,'r');
hold on;
plot(plot_a_time,plot_mod_velx,'r');
plot([inittime,inittime],[0,2]);
plot([finaltime,finaltime],[0,2]);