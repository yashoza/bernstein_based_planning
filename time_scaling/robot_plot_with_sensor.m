function[]=robot_plot_with_sensor(xo,yo,r,fill_bool)

x1=xo+(r*cos(0:0.01:2*pi));
y1=yo+(r*sin(0:0.01:2*pi));
%% plotting in the robot circle
if(fill_bool==1)
    fill(x1,y1,[11,3,11]/12);
end
plot(x1,y1,'g');
end