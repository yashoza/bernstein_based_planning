function [xo_cr,yo_cr,thetar]=get_circles(xo_cr_prev,yo_cr_prev,xo_crdot,yo_crdot,thetar_prev,omega_cr,delt,sc)
%% centre
xo_cr=xo_cr_prev+sc*xo_crdot*delt;
yo_cr=yo_cr_prev+sc*yo_crdot*delt;
thetar=thetar_prev+sc*omega_cr*delt;

end
