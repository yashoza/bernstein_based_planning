clear;
clc;

%{

Aim is to find the argmin of the function (J(smooth) + J(dist_to_goal))
We have state equations in the form :
x_next = x_current + v*cos(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)
y_next = y_current + v*sin(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)

x - coloumn vector that will store the successive values of x
y - coloumn vector that will store the successive values of y
theta - coloumn vector that will store the successive values for theta
theta_dot - coloumn vector that will store the successive values for theta_dot
theta_dotdot - coloumn vector that will store the successive values for theta_dotdot

%}


%% Initializations

% Specify the initial quantities of x(x(1,1)), y(y(1,1)), theta(theta(1,1)),
% theta_dot((theta_dot(1,1))), velocity (v(1,1)), n(number of points you have on the path) and delt

xo=0;
yo=0;
x(1,1) = xo;
y(1,1) = yo;
x_flag = xo;
y_flag = yo;
xf=180;
yf=180;
xg = Inf;
yg = Inf;
xc = 0;
yc = 0;
xodot = 0;
xoddot = 0;

delt = 0.1;
tst = 0;
tend = 20;
t_ar = tst:delt:tend;
t_ar = t_ar';
tc = 0;
t_size = size(t_ar);
t_size(1) = t_size(1) + 1;

theta(1,1) = atan(0);
theta_dot(1,1) = 0;
theta_sum_temp = 0;
theta_summation_x = 0;
theta_summation_y = 0;

plot_count = 0;
flag_cr = 0;
%d = 1;

ko = 0;
kodot = 0;
koddot = 0;
rand_counter = 0;

x_obs_1 = [46; 53; 60; 40; 46;];
y_obs_1 = [30; 32; 44; 55; 30;];

[centre_1,radius_1] = minboundcircle(x_obs_1,y_obs_1);

map_obs_1 = horzcat(x_obs_1, y_obs_1);