function [vel, om, k_mat, ko_mat, plot_xt_robo,plot_yt_robo ] = bernstein(xo,yo,xf,yf,xc,yc,xodot,xoddot,ko_val,kodot_val,tf)
disp('in bernstein_withx');
%% Simulation parameters
to = 0; % Simulation start time
tf = 20; % Simulation End time
delt=0.1; % Time step of simulation
endt = tf; % Extended End time
t=0; % Initial time
omega_prev = 0;

%% Path of robot
disp('calling create agent');
[x_weights_robo,tan_weights_robo] = create_Agent_func(xo,yo,xf,yf,xodot,xoddot,0,ko_val,kodot_val,0,0,0,0,20,xc,yc); % weights of robot 
%function [x_weights,tan_weights] = create_Agent_func(xo,yo,xf,yf,xodot,xoddot,xfdot,ko,kodot,koddot,kf,kfdot,to,tf,xc,yc)


ai = 1; % Index of of Current Path and orientation memory matrix  
al = 1.5; % Length of robo
ab = 1; % Width of robo
radiusrobo = sqrt(power((al/2),2)+power((ab/2),2)); % Radius of single circle encompassing robot
ahorizon = tf; % Total end time of robots simulation (depands on scaling) 
aprevscale =1; % Previous scale
acurrentscale = 1; % Current scale

%% Robots Memory vectors

thetar(1)=atan(0); % Initial-Heading is stored in Theta memory matrix
xo_cr(1)=xo; % Initial X-coordinate stored in X memory matrix                                   
yo_cr(1)=yo; % Initial Y-Coordinate stored in Y memory matrix

%% Plot of original path of Scaled robot
i=1;
for t=to:delt:tf
    [plot_xt_robo(i,:),plot_yt_robo(i,:),plot_velx(i,:),plot_vely(i,:),ko,kodot,omega,velocity(i, :), omega_dot]=get_robo(x_weights_robo,tan_weights_robo,yo_cr(1),to,t,tf, omega_prev, delt);
    %function [ xt, yt,xt_dot,yt_dot,kt,kt_dot,omega,velocity, omega_dot]=get_robo(x_weights,tan_weights,yo,to,t,tf, omega_prev, delt)

    X(i,1) = (plot_xt_robo(i,:));
    X(i,2) = (plot_yt_robo(i,:));
    %disp(omega_dot);
    %P(1,1) = (i)
    %P(1,2) = (omega_dot);
    P(i) = omega_dot;
    vel(i,:) = velocity(i,1);
    om(i,:) = omega;
    k_mat(i,:) = ko;
    ko_mat(i,:) = kodot;
    i=i+1;
end
t=0;
%yash2 = figure(2);
plot(plot_xt_robo,plot_yt_robo,'b');
%plot(P);
%disp(P);
end 