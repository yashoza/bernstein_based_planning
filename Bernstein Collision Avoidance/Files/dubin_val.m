function[final_pt] = dubin_val(centre,radius, bern_curve_a, bern_curve_b)
n_pts = 10;
j = 1;

a = [ bern_curve_a, bern_curve_b,0];
b = [280,280,0];
path = dubins_curve(a, b, 80, 0.1);
path_size = size(path);
%%disp('path_size');

pt1 = centre;
pt2 = [bern_curve_a,bern_curve_b];

for i = 1:n_pts 
    path_pts(i,:) = path(j,:);
    plot(path_pts(i,1),path_pts(i,2),'g*');
    A(i) = sqrt((path_pts(i,1) - pt1(1))*(path_pts(i,1) - pt1(1)) + (path_pts(i,2) - pt1(2))*(path_pts(i,2) - pt1(2)));
    c(i) = sqrt((path_pts(i,1) - pt2(1))*(path_pts(i,1) - pt2(1)) + (path_pts(i,2) - pt2(2))*(path_pts(i,2) - pt2(2)));
    j = j+ 240;
end

disp(path_size);
plot(path(:,1), path(:,2));

% for it = 1:path_size(1)
% %    disp('plotting dubin curve');
%     plot(path(it,1), path(it,2));
% end

% pt1 = centre;
% pt2 = [bern_curve(d,1),bern_curve(d,2)];

Aeq = ones(1,n_pts);
beq = 1;
%2.5,3.2
%plot(pt1(1,1),pt1(1,2),'b*');
%plot(pt2(1,1),pt2(1,2),'b*');

hold on;
b = radius + 60;
f = -c;

lb = zeros(size(f));
ub = lb + 1;
intvars = 1:length(f);

[x,~,~,~] = intlinprog(f,intvars,A,b,Aeq,beq,lb,ub);

for j = 1:size(x)
    if x(j) == 1
        final_pt = path_pts(j,:);
    end
end

%---------------------------------
% disp(final_pt);
% disp('reached goal, showing pt below ');
% disp(final_pt(1,1));
% disp(final_pt(1,2))
% plot(final_pt(1,1),final_pt(1,2),'r*');
%---------------------------------

% disp('reached goal');
% hold off;
end
