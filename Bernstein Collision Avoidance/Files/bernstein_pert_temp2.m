clear;
clc;

%{

Aim is to find the argmin of the function (J(smooth) + J(dist_to_goal))
We have state equations in the form :
x_next = x_current + v*cos(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)
y_next = y_current + v*sin(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)

x - coloumn vector that will store the successive values of x
y - coloumn vector that will store the successive values of y
theta - coloumn vector that will store the successive values for theta
theta_dot - coloumn vector that will store the successive values for theta_dot
theta_dotdot - coloumn vector that will store the successive values for theta_dotdot

%}


%% Initializations

% Specify the initial quantities of x(x(1,1)), y(y(1,1)), theta(theta(1,1)),
% theta_dot((theta_dot(1,1))), velocity (v(1,1)), n(number of points you have on the path) and delt

xo=0;
yo=0;
x(1,1) = xo;
y(1,1) = yo;
x_flag = xo;
y_flag = yo;
xf=180;
yf=180;
xg = Inf;
yg = Inf;
xc = 0;
yc = 0;
xodot = 0;
xoddot = 0;

delt = 0.1;
tst = 0;
tend = 20;
t_ar = tst:delt:tend;
t_ar = t_ar';
tc = 0;

theta(1,1) = atan(0);
theta_dot(1,1) = 0;
theta_sum_temp = 0;
theta_summation_x = 0;
theta_summation_y = 0;

plot_count = 0;
flag_cr = 0;

ko = 0;
kodot = 0;
koddot = 0;

%% Recursion Loop

% Loop to calculate theta_dotdot
% Loop repeats for the number of points you have on the path
% For now, doing it using the 'emphirical' formula.
% TODO - Should change it to iterative form

while (flag_cr == 0)
    
    % after yc it is tc, which im setting to 0
    [vel, om, k_mat, ko_mat]= bernstein(xo,yo,xf,yf,xc,yc, xodot, xoddot, ko, kodot, tend);
    size_vel = size(vel);
    vel_avg = sum(vel)/size_vel(1);
    %disp('vel_avg');
    %disp(vel_avg);
    
    %    disp('printing om and vel size ');
    %    disp(size(om));
    %    disp(size(vel));
    om = diff(om)/delt;
    
    t_size = size(t_ar);
    t_size(1) = t_size(1) + 1;
    
    %     vel(202,1) = 0.0;
    %     om(202,1) = 0.0;
    vel(t_size(1),1) = 0.0;
    om(t_size(1),1) = 0.0;
    
    theta_dotdot = om;
    acc = diff(vel)/delt;
    %    disp('printing final acc size');
    %    disp(size(acc));
    %
    %    disp('at start');
    
    for i = 1:t_size(1)
        
        for r = 1:i
            
            for j = 1:r
                theta_sum_temp = theta_sum_temp + (r-j+(1/2))*theta_dotdot(j,1)*delt*delt;
            end
            
            theta_calc = theta(1,1) + (r)*theta_dot(1,1)*delt + theta_sum_temp;
            theta_sum_temp = 0;
            theta_summation_x = theta_summation_x + vel(r,1)*cos((theta_calc))*delt;
            theta_summation_y = theta_summation_y + vel(r,1)*sin((theta_calc))*delt;
            
        end
        
        x(i+1,1) = x(1,1) + (theta_summation_x);
        y(i+1,1) = y(1,1) + (theta_summation_y);
        theta_summation_x = 0;
        theta_summation_y = 0;
    end
    
    x_obs = [46; 53; 60; 40; 46;];
    y_obs = [30; 32; 44; 55; 30;];
    
    [centre,radius] = minboundcircle(x_obs,y_obs);
    
    
    %      x_obs = [20; 16; 22; 30; 20;];
    %      y_obs = [10; 18; 27; 16; 10;];
    
    %      x_obs = [8; 6; 7.8; 10; 8;];
    %      y_obs = [2; 4; 5; 4; 2;];
    
    %      x_obs = [4; 6; 7.8; 5; 4;];
    %      y_obs = [6; 4; 5; 8; 6;];
    

    
    
    map_obs = horzcat(x_obs, y_obs);
    plot(x_obs, y_obs, 'r');
    hold on;
    
    if (plot_count == 0)
        plot(x,y, 'b');
        plot_count = plot_count  +1;
    end
    
    %disp(xg);
    %disp(yg);
    %disp(xf);
    %disp(yf);
    
    if (sqrt((xg-xf)*(xg-xf) + (yg-yf)*(yg-yf))<0.5)
        disp('reached goal ');
        disp('here');
        flag_cr = 1;
        break;
    end
    
    hold on;
    bern_curve = horzcat(x,y);
    
    size_tot = size(bern_curve);
    
    count = 0;
    flag = 0;
    %spl_pts = [];
    for d = 1:size_tot(1)
        [xy,distance,t] = distance2curve(map_obs,bern_curve(d,:),'linear');
        %disp(distance);
        if(distance < 42)
            %disp('Collision detecred. at x y ');
            %disp(bern_curve(d,1));
            %disp(bern_curve(d,2));
            plot(bern_curve(d,1),bern_curve(d,2),'r*') ;
            count = count + 1;
            spl_pts(1, :) = bern_curve(d,:);
            xoddot = acc(d);
            xodot = vel(d);
            ko = k_mat(d);
            kodot = ko_mat(d);
            flag = 1;
            xo = bern_curve(d,1);
            yo = bern_curve(d,2);
            xg = 180;
            yg = 180;
            %             xg = 20;
            %             yg = 20;
            
            break;
        end
    end
    %    disp(size(spl_pts));
    %    xo = spl_pts(1,1);
    %    yo = spl_pts(1,2);
    
    if(flag == 0)
        disp('coming to the flag = 0 loop');
        xo = xf;
        yo = yf;
    end
    
    
    %     prompt='Enter the X co-ordinate of the waypoint :';
    %     xf=input(prompt);
    %     prompt='Enter the X co-ordinate of the waypoint :';
    %     yf=input(prompt);
    final_pt = dubin_val(centre,radius,bern_curve(d,1),bern_curve(d,2));
   xc = final_pt(1,1);
   yc = final_pt(1,2);
    disp('final_pt');
    disp(final_pt);
    disp('xc');
    disp(final_pt(1,1));
    disp('yc');
    disp(final_pt(1,2));
%     prompt='Enter the X co-ordinate of the waypoint :';
%     xc=input(prompt);
%     prompt='Enter the X co-ordinate of the waypoint :';
%     yc=input(prompt);
    plot(xc,yc,'g*');
    %hold on;
    %     disp('at end xo xg yo yg');
    %       disp(xo);
    %       disp(xf);
    %       disp(yo);
    %       disp(yf);
    
    tot_length = sqrt((xo-x_flag)*(xo-x_flag) + (yo-y_flag)*(yo-y_flag)) + sqrt((xo-xg)*(xo-xg) + (yo-yg)*(yo-yg));
    %     tend = ((xo-xg)*(xo-xg) + (yo-yg)*(yo-yg))/vel_avg;
    %time_ratio = sqrt(((xo-xst)*(xo-xst) + (yo-yst)*(yo-yst))/((xo-xg)*(xo-xg) + (yo-yg)*(yo-yg)));
    %       disp('time_ratio');
    %       disp(tot_length);
    %FINAL ONE TO BE USED tc = (tend/tot_length)*sqrt((xo-xg)*(xo-xg) + (yo-yg)*(yo-yg));
    
    % disp(tc);
    
end

hold off;
