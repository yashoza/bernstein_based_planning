function [x_weights,tan_weights] = create_Agent_func(xo,yo,xf,yf,xodot,xoddot,xfdot,ko,kodot,koddot,kf,kfdot,to,tf,xc,yc)
disp('in CAF');
%create_Agent This function takes few initial and final conditions of robot
%and returns Weights
%   Details are commented in the Code Please have a look at it
%   Author: Barath and Mithun
%   Date: 12th June 2017
%   Licence: MIT License
%   Contact: mithunbabu1141995@gmail.com
%   Description of Input variables
%   xo - Inital X coordinate
%   yo - Inital Y coordinate
%   xf - Final X coordinate
%   yf - Final Y coordinate
%   xodot - Initial Xdot
%   xoddot - Initial Xdoubledot
%   xfdot - Final Xdot
%   ko - Initial Tan(init-heading)
%   kodot - Initial (Tan(init-heading))dot
%   koddot - Initial (Tan(init-heading))doubledot
%   kf - Final Tan(final-heading)
%   kfdot - Final (Tan(final-heading))dot
%   to - Initial time
%   tf - Fianl time

%% Weights calculation
disp('calling GW');
[x_weights,tan_weights]=get_weights(xo,yo,xf,yf,xodot,xoddot,xfdot,ko,kodot,koddot,kf,kfdot,to,tf, xc,yc);

end

