%%
%{
this is used to generate a bernstein path for only a single obstacle
the next code for multi obstacle is bern_robo_mult
%}

%%

clear;
clc;

%{

Aim is to find the argmin of the function (J(smooth) + J(dist_to_goal))
We have state equations in the form :
x_next = x_current + v*cos(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)
y_next = y_current + v*sin(theta_prev + theta_prev_dot*delt + (1/2)*theta_prev_dot_dot*delt*delt)

x - coloumn vector that will store the successive values of x
y - coloumn vector that will store the successive values of y
theta - coloumn vector that will store the successive values for theta
theta_dot - coloumn vector that will store the successive values for theta_dot
theta_dotdot - coloumn vector that will store the successive values for theta_dotdot

%}


%% Initializations

% Specify the initial quantities of x(x(1,1)), y(y(1,1)), theta(theta(1,1)),
% theta_dot((theta_dot(1,1))), velocity (v(1,1)), n(number of points you have on the path) and delt

xo=0;
yo=0;
x(1,1) = xo;
y(1,1) = yo;
x_flag = xo;
y_flag = yo;
xf=180;
yf=180;
xg = Inf;
yg = Inf;
xc = 0;
yc = 0;
xodot = 0;
xoddot = 0;

delt = 0.1;
tst = 0;
tend = 20;
t_ar = tst:delt:tend;
t_ar = t_ar';
tc = 0;
t_size = size(t_ar);
t_size(1) = t_size(1) + 1;

theta(1,1) = atan(0);
theta_dot(1,1) = 0;
theta_sum_temp = 0;
theta_summation_x = 0;
theta_summation_y = 0;

plot_count = 0;
flag_1 = 0;
%d = 1;

ko = 0;
kodot = 0;
koddot = 0;

% x_obs_1 = [46 ; 53 + 20; 60 + 20; 40 ; 46 ;];
% y_obs_1 = [30 + 20; 32 ; 44 ; 55 + 20; 30 + 20;];

x_obs_1 = [46; 53; 60; 40; 46;];
y_obs_1 = [30; 32; 44; 55; 30;];
x_obs_2 = [125 - 40; 140 - 40; 115 - 20; 110 - 20; 125 - 40; ];
y_obs_2 = [85 - 40; 110 - 40; 120 - 20; 105 - 20; 85 - 40; ];

[centre_1,radius_1] = minboundcircle(x_obs_1,y_obs_1);
[centre_2,radius_2] = minboundcircle(x_obs_2,y_obs_2);

map_obs_1 = horzcat(x_obs_1, y_obs_1);
map_obs_2 = horzcat(x_obs_2, y_obs_2);

%% Recursion Loop

% Loop to calculate theta_dotdot
% Loop repeats for the number of points you have on the path
% For now, doing it using the 'emphirical' formula.
% TODO - Should change it to iterative form

yash  = figure(6);
hold on;

theta = linspace(0,2*pi,50);
axis([0 300 0 300]);
locx = centre_1(1,1);
locy = centre_1(1,2);
temdist = 1000;
simtime = 1;
varx = 1;
flag_final = 0;

while(simtime < t_size(1))
    figure(6);
    cla;
    
    disp('calling bernstein_withx');
    
    [vel, omega, k_mat, ko_mat, plot_xt_robo,plot_yt_robo] = bernstein_withx(xo,yo,xf,yf,xc,yc,xodot,xoddot, ko, kodot, tend);
    
    bern_curve = horzcat(plot_xt_robo,plot_yt_robo);
    size_tot = size(bern_curve);
    
    size_vel = size(vel);
    vel_avg = sum(vel)/size_vel(1);
    omega = diff(omega)/delt;
    
    vel(t_size(1),1) = 0.0;
    omega(t_size(1),1) = 0.0;
    
    theta_dotdot = omega;
    acc = diff(vel)/delt;
    
    plot(plot_xt_robo(varx,:)+2*cos(theta),plot_yt_robo(varx,:)+2*sin(theta),'r');
    figure(yash);
    plot(x_obs_1, y_obs_1, 'r');
    plot(x_obs_2, y_obs_2, 'r');
    
    disp((simtime + 45));
    disp(t_size(1));
    
    if((simtime + 45) < 202)
        [xy,distance_1,t] = distance2curve(map_obs_1,bern_curve((simtime + 45),:),'linear');
        [xy,distance_2,t] = distance2curve(map_obs_2,bern_curve((simtime + 45),:),'linear');
    
    end
    
    if((distance_1 < 2) | (distance_2 < 2) )

%    if((distance_1 < 2))
        if (distance_1 < 2)
            flag_1 = 1;
        end
        disp('obstacle detected ! replanning now ')
        spl_pts(1, :) = bern_curve(simtime,:);
        xoddot = acc(simtime);
        xodot = vel(simtime);
        ko = k_mat(simtime);
        kodot = ko_mat(simtime);
        xo = bern_curve(simtime,1);
        yo = bern_curve(simtime,2);
        xg = 180;
        yg = 180;
        tic;
        if (flag_1 == 1)
            final_pt = dubin_val(centre_1,radius_1,bern_curve(simtime,1),bern_curve(simtime,2));
        else
            final_pt = dubin_val(centre_2,radius_2,bern_curve(simtime,1),bern_curve(simtime,2)); 
        end
        flag_final = 1;
        xc = final_pt(1,1);
        yc = final_pt(1,2);
        simtime = 1;
        varx = 1;
        disp('new path without obstacle found');
        tim = toc;
        disp('replanning time is (in seconds)');
        disp(tim);
        disp('frequency of replanning is (in hertz)');
        disp(1/tim);
    end
    %end
    if flag_final > 0 
        plot(final_pt(1,1),final_pt(1,2),'r*');
    end
    
    [vel, omega, k_mat, ko_mat, plot_xt_robo,plot_yt_robo ] = bernstein_withx(xo,yo,xf,yf,xc,yc,xodot,xoddot, ko, kodot, tend);
    
    varx =varx +1;
    simtime = simtime + 1;
    pause(0.1);
    
end

hold off;