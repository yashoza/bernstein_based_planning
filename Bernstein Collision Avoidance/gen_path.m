yash  = figure(6);
hold on;

theta = linspace(0,2*pi,50);
axis([0 250 0 250]);
simtime = 1;
varx = 1;

while(simtime < t_size(1))
    figure(6);
    cla;
    
    [vel, omega, k_mat, ko_mat, plot_xt_robo,plot_yt_robo] = bernstein_withx(xo,yo,xf,yf,xc,yc,xodot,xoddot, ko, kodot, tend);
    
    bern_curve = horzcat(plot_xt_robo,plot_yt_robo);
    %size_tot = size(bern_curve);
    
    size_vel = size(vel);
    vel_avg = sum(vel)/size_vel(1);
    omega = diff(omega)/delt;
    
    vel(t_size(1),1) = 0.0;
    omega(t_size(1),1) = 0.0;

    theta_dotdot = omega;
    acc = diff(vel)/delt;

    plot(plot_xt_robo(varx,:)+2*cos(theta),plot_yt_robo(varx,:)+2*sin(theta),'r');
    figure(yash);
    plot(x_obs_1, y_obs_1, 'r');
    
    disp((simtime + 35));
    disp(t_size(1));
    if((simtime + 35) < t_size(1))
        [xy,distance,t] = distance2curve(map_obs_1,bern_curve((simtime + 35),:),'linear');
    end
    
    %if(rand_counter < 1)
        %if(distance < 42)
        if(distance < 2 )        
            disp('obstacle detected ! replanning now ')
            %rand_counter = rand_counter + 1;
            spl_pts(1, :) = bern_curve(simtime,:);
            xoddot = acc(simtime);
            xodot = vel(simtime);
            ko = k_mat(simtime);
            kodot = ko_mat(simtime);
            xo = bern_curve(simtime,1);
            yo = bern_curve(simtime,2);
            xg = 180;
            yg = 180;
            tic;            
            final_pt = dubin_val(centre_1,radius_1,bern_curve(simtime,1),bern_curve(simtime,2));
            xc = final_pt(1,1);
            yc = final_pt(1,2);
            simtime = 1;          
            varx = 1;
            disp('new path without obstacle found');
            tim = toc;
            disp('replanning time is (in seconds)');
            disp(tim);
            disp('frequency of replanning is (in hertz)');
            disp(1/tim);
        end
    %end
    
    
    [vel, omega, k_mat, ko_mat, plot_xt_robo,plot_yt_robo ] = bernstein_withx(xo,yo,xf,yf,xc,yc,xodot,xoddot, ko, kodot, tend);
    
    varx =varx +1;
    simtime = simtime + 1;
    pause(0.1);
    
end

hold off;
